#!/usr/bin/env python3

import os
import sys
import cgi
import cgitb
import codecs
import html
import http.cookies
import re

import psycopg2
from psycopg2 import sql


conn = psycopg2.connect(
    dbname='postgres',
    user='myuser',
    password='UigfjL0f',
    host='localhost'
)
k1 = 0
k2 = 0
k3 = 0
d = []
with conn.cursor() as cursor:
    conn.autocommit = True
    try:
        insert = f"SELECT * FROM form"
        cursor.execute(insert)
        form = cursor.fetchall()
        for row in form:
            strrow = ''.join(row[6])
            strrow = strrow.replace('{', '')
            strrow = strrow.replace('}', '')
            strrow = strrow.replace('"', '')
            b = strrow.split(',')
            d.extend(b)
    except psycopg2.Error:
        f = True
        form = None
conn.close()

if form is not None:
    print("Content-type: text/html")
    print(f'''
        <!DOCTYPE html>
        <html lang="ru">
            <head>
                <title>Админка</title>
                <meta charset="UTF-8">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
            </head>
            <body>''')
    for i in form:
        print(f'''{i}<br>
            <a class="btn btn-danger" href="/admin/delete.html">Удалить ответ</a>
            <a class="btn btn-primary" href="/admin/change.html">Редактировать ответ</a><br>
            ''')
    print(f'''
            <table>
                <tr>
                    <th>Бессметрие</th>
                    <th>Прохождение через стены</th>
                    <th>Левитация</th>
                </tr>
                <tr>
                    <td>{d.count('Бессмертие')}</td>
                    <td>{d.count('Прохождение через стены')}</td>
                    <td>{d.count('Левитация')}</td>
                </tr>
            </table>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
            </body>
        </html>
    ''')

